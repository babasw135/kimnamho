package com.bbs.kimnamho.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Humanity {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friendid", nullable = false)
    private Friend friend;
    @Column(nullable = false)
    private Boolean type;
    @Column(columnDefinition = "TEXT")
    private String giftType;
    @Column(nullable = false)
    private Double amount;
    @Column(nullable = false)
    private LocalDate day;
}
