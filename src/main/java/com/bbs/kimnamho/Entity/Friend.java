package com.bbs.kimnamho.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Length;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Friend {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private LocalDate birthDate;
    @Column(nullable = false)
    private String phoneNumber;
    @Column(columnDefinition = "TEXT")
    private String etcMemo;
    @Column(nullable = false)
    private Boolean isCutOff;

    private LocalDate cutDate;
    @Column(length = 50)
    private String reason;
}
