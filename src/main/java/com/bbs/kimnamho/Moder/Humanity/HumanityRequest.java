package com.bbs.kimnamho.Moder.Humanity;

import com.bbs.kimnamho.Entity.Friend;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HumanityRequest {
    private Boolean type;
    private String giftType;
    private Double amount;
}
