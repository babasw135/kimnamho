package com.bbs.kimnamho.Moder.Friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendRequest {
    private String name;
    private LocalDate birthDate;
    private String phoneNumber;
    private String etcMemo;
}
