package com.bbs.kimnamho.Moder.Friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutUpdateRequest {
    private String reason;
}
