package com.bbs.kimnamho.Controller;

import com.bbs.kimnamho.Moder.Friend.FriendCutUpdateRequest;
import com.bbs.kimnamho.Moder.Friend.FriendRequest;
import com.bbs.kimnamho.service.FriendService;
import com.bbs.kimnamho.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/friend")
public class FriendController {
    private final FriendService friendService;
    private final HumanityService humanityService;

    @PostMapping("/new")
    public String setFriend(@RequestBody FriendRequest request){
    friendService.setFriend(request);

    return "ok";
    }

    @PutMapping("/input")
    public String putFriendCut(@PathVariable long id, @RequestBody FriendCutUpdateRequest request){
    friendService.putFriendCut(id, request);

    return "ok";
    }
}
