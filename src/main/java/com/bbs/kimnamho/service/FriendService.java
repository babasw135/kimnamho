package com.bbs.kimnamho.service;


import com.bbs.kimnamho.Entity.Friend;
import com.bbs.kimnamho.Moder.Friend.FriendCutUpdateRequest;
import com.bbs.kimnamho.Moder.Friend.FriendRequest;
import com.bbs.kimnamho.repository.FriendRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public void setFriend(FriendRequest request){
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutOff(false);

        friendRepository.save(addData);
    }
    public void putFriendCut(long id, FriendCutUpdateRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCutOff(true);
        originData.setCutDate(LocalDate.now());
        originData.setReason(request.getReason());

        friendRepository.save(originData);
    }
}
