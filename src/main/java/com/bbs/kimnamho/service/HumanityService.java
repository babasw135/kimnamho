package com.bbs.kimnamho.service;

import com.bbs.kimnamho.Entity.Friend;
import com.bbs.kimnamho.Entity.Humanity;
import com.bbs.kimnamho.Moder.Humanity.HumanityRequest;
import com.bbs.kimnamho.repository.HumanityRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHuman(HumanityRequest request){
        Humanity addData = new Humanity();
        addData.setType(request.getType());
        addData.setGiftType(request.getGiftType());
        addData.setAmount(request.getAmount());

        humanityRepository.save(addData);
    }
}
