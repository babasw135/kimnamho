package com.bbs.kimnamho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KimNamhoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KimNamhoApplication.class, args);
	}

}
