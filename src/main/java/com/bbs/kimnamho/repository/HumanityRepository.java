package com.bbs.kimnamho.repository;

import com.bbs.kimnamho.Entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity, Long> {
}
