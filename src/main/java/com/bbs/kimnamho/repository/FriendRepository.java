package com.bbs.kimnamho.repository;

import com.bbs.kimnamho.Entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend,Long> {
}
